package com.niti.evaluationservice.controller;

import com.niti.evaluationservice.model.EvaluateSignal;
import com.niti.evaluationservice.service.EvaluateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/evaluate")
public class EvaluateController {

    private final EvaluateService evaluateService;

    public EvaluateController(EvaluateService evaluateService) {
        this.evaluateService = evaluateService;
    }

    @GetMapping("/test")
    public String test() {
        return "Test";
    }

    @GetMapping("/{assetId}")
    public ResponseEntity<EvaluateSignal> evaluateOpportunity(@PathVariable String assetId) {
        EvaluateSignal evaluateSignal = evaluateService.evaluateOpportunity(assetId);
        return ResponseEntity.ok(evaluateSignal);
    }
}
