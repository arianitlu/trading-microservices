# Trading Microservices

This project contains a set of microservices that work together to form a trading platform. Each microservice is responsible for a specific piece of functionality within the platform.

## Microservices

- **asset-service**: Manages the assets being traded on the platform.
- **email-service**: Handles sending notifications and alerts to users.
- **eureka-server**: Service registry for registering all microservices and enabling service discovery.
- **evaluation-service**: Provides evaluations and analytics for trading assets.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them:

- Java JDK 17 or higher
- Maven
- Docker (optional for containerization)

### Installing

A step by step series of examples that tell you how to get a development environment running.

```bash
# Clone the repository
git clone https://gitlab.com/arianitlu/trading-microservices.git

# Navigate to each microservice directory
cd [microservice-name]

# Build and run each microservice
mvn spring-boot:run