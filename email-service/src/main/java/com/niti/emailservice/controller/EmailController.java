package com.niti.emailservice.controller;

import com.niti.emailservice.service.EmailService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    private EmailService emailService;

    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping("/send")
    public String sendEmail(@RequestParam String to,
                            @RequestParam String subject,
                            @RequestParam String text) {
        boolean isSent = emailService.sendSimpleMessage(to, subject, text);
        if (isSent){
            return "Email sent successfully";
        } else {
            return "Failed to send email";
        }
    }
}
