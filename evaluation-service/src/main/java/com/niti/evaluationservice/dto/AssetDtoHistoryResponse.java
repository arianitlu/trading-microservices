package com.niti.evaluationservice.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AssetDtoHistoryResponse {

    private List<AssetHistory> data;

    @Getter
    @Setter
    public static class AssetHistory {
        private String priceUsd;
        private long time;
    }
}
