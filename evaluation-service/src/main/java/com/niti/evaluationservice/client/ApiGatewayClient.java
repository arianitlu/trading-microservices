package com.niti.evaluationservice.client;

import com.niti.evaluationservice.dto.AssetDto;
import com.niti.evaluationservice.dto.AssetDtoHistoryResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "api-gateway")
public interface ApiGatewayClient {

    @GetMapping("/assets/asset/{id}")
    AssetDto getAssetById(@PathVariable("id") String assetId);

    @GetMapping("/assets/asset/{id}/history")
    AssetDtoHistoryResponse getAssetHistory(@PathVariable("id") String assetId, @RequestParam("interval") String interval);

    @PostMapping("/emails/send")
    String sendEmail(@RequestParam("to") String to,
                     @RequestParam("subject") String subject,
                     @RequestParam("text") String text);
}
