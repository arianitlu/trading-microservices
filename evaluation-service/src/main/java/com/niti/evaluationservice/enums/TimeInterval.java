package com.niti.evaluationservice.enums;

public enum TimeInterval {
    M1("m1"),
    M5("m5"),
    M15("m15"),
    M30("m30"),
    H1("h1"),
    H2("h2"),
    H6("h6"),
    H12("h12"),
    D1("d1");

    private final String interval;

    TimeInterval(String interval) {
        this.interval = interval;
    }

    public String getInterval() {
        return interval;
    }
}
