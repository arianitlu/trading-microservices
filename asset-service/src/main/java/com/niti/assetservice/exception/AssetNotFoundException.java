package com.niti.assetservice.exception;

public class AssetNotFoundException extends RuntimeException{

    public AssetNotFoundException(String message) {
        super(message);
    }
}
