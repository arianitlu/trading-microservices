package com.niti.evaluationservice.service;

import com.niti.evaluationservice.algorithm.SimplePercentageDropAlgorithm;
import com.niti.evaluationservice.client.ApiGatewayClient;
import com.niti.evaluationservice.dto.AssetDto;
import com.niti.evaluationservice.dto.AssetDtoHistoryResponse;
import com.niti.evaluationservice.enums.TimeInterval;
import com.niti.evaluationservice.model.EvaluateSignal;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EvaluateService {

    private final ApiGatewayClient apiGatewayClient;
    private final SimplePercentageDropAlgorithm tradingAlgorithm;


    public EvaluateService(ApiGatewayClient apiGatewayClient, RestTemplate restTemplate) {
        this.apiGatewayClient = apiGatewayClient;
        this.tradingAlgorithm = new SimplePercentageDropAlgorithm();
    }

    public EvaluateSignal evaluateOpportunity(String assetId) {

        AssetDto currentAsset = apiGatewayClient.getAssetById(assetId);
        AssetDtoHistoryResponse assetHistory = apiGatewayClient.getAssetHistory(assetId, TimeInterval.D1.getInterval());

        String price24HrsAgoStr = extractPriceFrom24HrsAgo(assetHistory);

        EvaluateSignal evaluateSignal = tradingAlgorithm.evaluate(assetId, currentAsset.getName(), currentAsset.getPriceUsd(), price24HrsAgoStr);

        if (evaluateSignal.getSignalType() == EvaluateSignal.SignalType.BUY || evaluateSignal.getSignalType() == EvaluateSignal.SignalType.SELL){
            //sendNotificationEmail(evaluateSignal);
        }

        return evaluateSignal;
    }

    private String extractPriceFrom24HrsAgo(AssetDtoHistoryResponse assetHistory) {
        if (assetHistory == null || assetHistory.getData() == null || assetHistory.getData().isEmpty()) {
            return null;
        }

        // Assuming the list is ordered by time, and the last entry is the latest,
        // and the second last entry represents data from 24 hours ago.
        AssetDtoHistoryResponse.AssetHistory history24HrsAgo =
                assetHistory.getData().get(assetHistory.getData().size() - 2);

        return history24HrsAgo.getPriceUsd();

    }

    private void sendNotificationEmail(EvaluateSignal signal) {
        String emailSubject = "Trading Signal Alert: " + signal.getSignalType();
        String emailBody = String.format("Signal for %s (%s): %s. Current price: %s, Price 24 hours ago: %s",
                signal.getAssetName(), signal.getAssetId(),
                signal.getSignalType(), signal.getCurrentPrice(),
                signal.getPrice24HrsAgo());

        String recipientEmail = "arianit.lu@gmail.com";

        String emailSentText = apiGatewayClient.sendEmail(recipientEmail, emailSubject, emailBody);

        emailSentText.getClass();
    }
}
