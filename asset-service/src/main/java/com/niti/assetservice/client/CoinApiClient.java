package com.niti.assetservice.client;

import com.niti.assetservice.dto.AssetDto;
import com.niti.assetservice.dto.AssetDtoHistoryResponse;
import com.niti.assetservice.dto.AssetDtoMarketResponse;
import com.niti.assetservice.dto.AssetDtoResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class CoinApiClient {

    private static final String BASE_URL = "https://api.coincap.io/v2";

    private final RestTemplate restTemplate;

    public CoinApiClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<AssetDto> getAllAssets() {
        AssetDtoResponse.AssetsList response = restTemplate.getForObject(BASE_URL + "/assets", AssetDtoResponse.AssetsList.class);
        return response.getAssetDtos();
    }

    public AssetDto getAssetById(String id) {
        AssetDtoResponse.SingleAsset response = restTemplate.getForObject(BASE_URL + "/assets/" + id, AssetDtoResponse.SingleAsset.class);
        return response.getAssetdto();
    }

    public AssetDtoHistoryResponse getAssetHistory(String id, String interval) {
        String url = BASE_URL + "/assets/" + id + "/history";
        if (interval != null && !interval.isEmpty()) {
            url += "?interval=" + interval;
        }
        return restTemplate.getForObject(url, AssetDtoHistoryResponse.class);
    }

    public AssetDtoMarketResponse getAssetMarkets(String id) {
        return restTemplate.getForObject(BASE_URL + "/assets/" + id + "/markets", AssetDtoMarketResponse.class);
    }
}
