package com.niti.evaluationservice.algorithm;

import com.niti.evaluationservice.model.EvaluateSignal;

public class SimplePercentageDropAlgorithm {

    private static final double THRESHOLD = 0.0005; // 5%

    public EvaluateSignal evaluate(String assetId, String assetName, String currentPriceStr, String price24HrsAgoStr) {
        double currentPrice = Double.parseDouble(currentPriceStr);
        double price24HrsAgo = Double.parseDouble(price24HrsAgoStr);

        double priceChange = (currentPrice - price24HrsAgo) / price24HrsAgo;

        /* Simple strategy
        - If the price has dropped by 5% or more, recommend a SELL.
        - If the price has risen by 5% or more, recommend a BUY.
        - Otherwise, recommend a HOLD. */

        if (priceChange >= THRESHOLD) {
            return new EvaluateSignal(assetId, assetName, EvaluateSignal.SignalType.BUY,
                    currentPriceStr, price24HrsAgoStr);
        } else if (priceChange <= -THRESHOLD) {
            return new EvaluateSignal(assetId, assetName, EvaluateSignal.SignalType.SELL,
                    currentPriceStr, price24HrsAgoStr);
        } else {
            return new EvaluateSignal(assetId, assetName, EvaluateSignal.SignalType.HOLD,
                    currentPriceStr, price24HrsAgoStr);
        }

    }

}
