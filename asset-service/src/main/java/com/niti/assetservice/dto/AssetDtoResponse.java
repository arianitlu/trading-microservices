package com.niti.assetservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class AssetDtoResponse {
    @Getter
    @Setter
    public static class AssetsList {
        @JsonProperty("data")
        private List<AssetDto> assetDtos;

        @JsonProperty("timestamp")
        private Long timestamp;
    }

    @Getter
    @Setter
    public static class SingleAsset {
        @JsonProperty("data")
        private AssetDto assetdto;

        @JsonProperty("timestamp")
        private Long timestamp;
    }
}
