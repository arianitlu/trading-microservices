package com.niti.assetservice.controller;

import com.niti.assetservice.dto.AssetDto;
import com.niti.assetservice.dto.AssetDtoHistoryResponse;
import com.niti.assetservice.dto.AssetDtoMarketResponse;
import com.niti.assetservice.exception.AssetNotFoundException;
import com.niti.assetservice.service.AssetService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/asset")
public class AssetController {

    private final AssetService assetService;

    public AssetController(AssetService assetService) {
        this.assetService = assetService;
    }

    @GetMapping("")
    public ResponseEntity<List<AssetDto>> getAllAssets() {
        List<AssetDto> assets = assetService.getAllAssets();
        if (assets.isEmpty()) {
            throw new AssetNotFoundException("No assets found.");
        }
        return ResponseEntity.ok(assets);
    }

    @PostMapping("/save")
    public ResponseEntity<List<AssetDto>> saveAssets() {
        List<AssetDto> assets = assetService.saveAllAssets();
        if (assets.isEmpty()) {
            throw new AssetNotFoundException("No assets found.");
        }
        return ResponseEntity.ok(assets);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AssetDto> getAssetById(@PathVariable String id) {
        AssetDto asset = assetService.getAssetById(id);
        if (asset == null) {
            throw new AssetNotFoundException("Asset with ID: " + id + " not found.");
        }
        return ResponseEntity.ok(asset);
    }

    @GetMapping("/{id}/history")
    public ResponseEntity<AssetDtoHistoryResponse> getAssetHistory(@PathVariable String id,
                                                                   @RequestParam(required = false) String interval) {
        AssetDtoHistoryResponse assetHistory = assetService.getAssetHistory(id, interval);
        if (assetHistory == null) {
            throw new AssetNotFoundException("History for asset with ID: " + id + " not found.");
        }
        return ResponseEntity.ok(assetHistory);
    }

    @GetMapping("/{id}/markets")
    public ResponseEntity<AssetDtoMarketResponse> getAssetMarkets(@PathVariable String id) {
        AssetDtoMarketResponse assetMarkets = assetService.getAssetMarkets(id);
        if (assetMarkets == null) {
            throw new AssetNotFoundException("Markets for asset with ID: " + id + " not found.");
        }
        return ResponseEntity.ok(assetMarkets);
    }
}
